﻿using UnityEngine;
using System.Collections;
using System.Net;
using UnityEngine.Networking;
using System.IO;
using UnityEngine.UI;

public class AssetDownloader : MonoBehaviour
{
    [SerializeField]
    private GameObject ErrorPanel;
    [SerializeField]
    private Text ErrorText;
    private string internetErrorMess = "Ошибка подключения к интернету";
    private string assetErrorMess = "Ошибка загрузки модели";
    private string androidLebel = ".android";
    private string iosLebel = ".ios";
    private string name = "testcube";
    private string root = "http://202905.selcdn.com/dr_roofs/";
    private string address;
    


    void Start()
    {
        string HtmlText = GetHtmlFromUri("http://google.com");
        if (HtmlText == "")
        {
            ErrorPanel.SetActive(true);
            ErrorText.text = internetErrorMess;
        }
        else
        {
            ErrorPanel.SetActive(false);
            //StartCoroutine(GetAssetBundle());
        }

        
    }

    /*IEnumerator GetAssetBundle()
    {
        //for testng in unity
        //if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.Android)
        //    address = root + name + androidLebel;
        //else if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.iOS)
        //    address = root + name + iosLebel;

        if(Application.platform == RuntimePlatform.Android)
            address = root + name + androidLebel;
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
            address = root + name + iosLebel;

        UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(address);

        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            ErrorPanel.SetActive(true);
            ErrorText.text = internetErrorMess;
        }
        else
        {
            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www);
            var cube = bundle.LoadAsset("TestCube");
            Instantiate(cube);
        }
    }*/

    public string GetHtmlFromUri(string resource)
    {
        string html = string.Empty;
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
        try
        {
            using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
            {
                bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                if (isSuccess)
                {
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                    {
                        //We are limiting the array to 80 so we don't have
                        //to parse the entire html document feel free to 
                        //adjust (probably stay under 300)
                        char[] cs = new char[80];
                        reader.Read(cs, 0, cs.Length);
                        foreach (char ch in cs)
                        {
                            html += ch;
                        }
                    }
                }
            }
        }
        catch
        {
            return "";
        }
        return html;
    }
}