﻿using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class NativeBridge : MonoBehaviour
{
    [SerializeField]
    private Toggle toggle;

    static AssetController myAsset;

    private bool skipToggleChangeEvent;




#if UNITY_IOS && !UNITY_EDITOR
    [DllImport("__Internal")]
    private extern static void UnityToggleRotation(bool isOn);
#else


    private void UnityToggleRotation(bool isOn)
    {
        RotateCube(isOn ? "start" : "stop");
    }
#endif

    void Awake()
    {
        //StartCoroutine(AssetController.I.GetAssetBundle());
        myAsset = new AssetController();
        Debug.Log("Empty Asset Created");
        //Rigidbody rb = myAsset.SetRigidbody();
        Debug.Log("Calling to GetAsset methot");
        StartCoroutine(myAsset.GetAssetBundle());
        Debug.Log("And of Awake");

    }

        public void OnToggleValueChanged(bool isOn)
    {
        if (!skipToggleChangeEvent)
        {

            UnityToggleRotation(isOn);

        }

        myAsset.ShouldRotate = isOn;
        AssetController.I.ShouldRotate = isOn;
        //CubeController.I.ShouldRotate = isOn;
    }

    private void RotateCube(string command)
    {
        switch (command)
        {
            case "start":
                myAsset.ShouldRotate = true;
                AssetController.I.ShouldRotate = true;
                //CubeController.I.ShouldRotate = true;
                break;
            case "stop":
                myAsset.ShouldRotate = false;
                AssetController.I.ShouldRotate = false;
                //CubeController.I.ShouldRotate = false;
                break;
        }

        skipToggleChangeEvent = true;
        //toggle.isOn = myAsset.ShouldRotate; // CubeController.I.ShouldRotate;
        toggle.isOn = AssetController.I.ShouldRotate;
        skipToggleChangeEvent = false;
    }
}
