﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.IO;

public class AssetController : MonoBehaviour {

    //private string androidLebel = ".android";
    private string iosLebel = ".ios";
    private string name = "cubesphere2";
    private string modelName = "TestCube";
    private string root = "https://bwagroupcdn-a.akamaihd.net/";// "https://202905.selcdn.com/dr_roofs/";
    private string address;

    public static AssetController I { get; private set; }
    public static GameObject myinstanse { get; private set; }

    public bool ShouldRotate { get; set; }

    private Rigidbody rigibody { get; set; }

    private float torqueTime;

    private Vector3 torque;

    private void Awake()
    {
        /*string HtmlText = GetHtmlFromUri("http://google.com");
        if (HtmlText == "")
        {
            Debug.Log("Ошибка подключения к интернету");
        }
        else
        {
            //ErrorPanel.SetActive(false);
            StartCoroutine(GetAssetBundle());
            */
        //StartCoroutine(GetAssetBundle());
        I = this;
        rigibody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (ShouldRotate)
        {
            rigibody.angularDrag = 0.05f;
        }
        else
        {
            rigibody.angularDrag = 1f;
        }
    }

    private void FixedUpdate()
    {
        if (ShouldRotate)
        {
            UpdateTorque();
        }
    }

    private void UpdateTorque()
    {
        if (torqueTime <= 0)
        {
            torqueTime = Random.Range(0.5f, 2f);
            torque = Random.insideUnitSphere * Random.Range(1f, 3f);
        }

        torqueTime -= Time.fixedDeltaTime;

        rigibody.AddTorque(torque, ForceMode.Force);
    }

    public IEnumerator GetAssetBundle()
    {
        Debug.Log("Start of GetAssetBundle");
        address = root + name + iosLebel;
        I = this;
        using (WWW www = new WWW(address))
        {
            Debug.Log("Waiting for www");
            yield return www;
            Debug.Log("WWW done");
            if (www.error != null)
                Debug.Log("WWW download had an error:" + www.error 
                    + "; Error type:" + www.error.GetType().ToString() + "; " 
                    + www.error.GetTypeCode());
            else
                Debug.Log("Geting Asset Bundle from www");
            AssetBundle bundle = www.assetBundle;
            var cube = bundle.LoadAsset(modelName);
            Debug.Log("Cube loaded from bundle");
            myinstanse = Instantiate(cube) as GameObject;
            Debug.Log("Cube instantiated" + myinstanse);
            myinstanse.AddComponent<AssetController>();
            rigibody = myinstanse.GetComponent<Rigidbody>();
            Debug.Log("Rigibody set " + rigibody);
            // Unload the AssetBundles compressed contents to conserve memory
            bundle.Unload(false);

            //UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(address);
            //yield return www.SendWebRequest();
            //if (www.isNetworkError || www.isHttpError)
            //{
            //    Debug.Log("Ошибка загрузки модели");
            //}
            //else
            //{
            //    AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www);
            //    var cube = bundle.LoadAsset("TestCube");
            //    myinstanse = Instantiate(cube) as GameObject;
            //    myinstanse.AddComponent<AssetController>();
            //    rigibody = myinstanse.GetComponent<Rigidbody>();
            //    rigibody.AddTorque(Vector3.back);

            //}
        }
    }


    public string GetHtmlFromUri(string resource)
    {
        string html = string.Empty;
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
        try
        {
            using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
            {
                bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                if (isSuccess)
                {
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                    {
                        //We are limiting the array to 80 so we don't have
                        //to parse the entire html document feel free to 
                        //adjust (probably stay under 300)
                        char[] cs = new char[80];
                        reader.Read(cs, 0, cs.Length);
                        foreach (char ch in cs)
                        {
                            html += ch;
                        }
                    }
                }
            }
        }
        catch
        {
            return "";
        }
        return html;
    }


}
